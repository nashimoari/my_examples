<?php
namespace workflow;
if (!defined('index_constant')) die ('Not a valid entry point');
use \Exception;

class ccontext {
  private $wf_data;
  private $db;
  private $workflow_id = -1;
  private $is_end = false;
  /**
   * @var State link to current state of context.
   */
  private $state;
  
function __construct() {
  $this->db = new \cdb();
}

public function step_process() {
  $pid = getmypid();

  $sql['sql'] = 'update workflow set handled_dt = sysdate(), handler_pid = :pid, state_id=2, last_action_dt=sysdate() where state_id = 1 and td is null and handled_dt is null and handler_pid is null';
  $sql['var']['pid'] = $pid;
  $tmp_res = $this->db->exec($sql);
  unset($sql);
  if ($tmp_res['code']!=1) {
    throw new Exception("Error update workflow");
  }


  $sql['sql'] = 'select * from workflow wf where wf.state_id = 2 and td is null and handled_dt is not null and handler_pid = :pid';
  $sql['var']['pid'] = $pid;
  $tmp_res = $this->db->exec($sql);
  if ($tmp_res['code']!=1) {
    throw new Exception("Error update workflow");
  }

  $wf_arr = $tmp_res['values'];

  if (count($wf_arr)==0) {
    return;
  }

  foreach($wf_arr as $wf_curr) {
    // set context wf data
    $this->wf_data = json_decode($wf_curr['json_data'],true);
    $this->is_end = false;
    $this->workflow_id = $wf_curr['workflow_id'];

    // init wf by type_id
    $this->initialize_by_type_id($wf_curr['type_id']);

    // execute workflow
    $new_state_id = 1;
    $tmp_res = $this->execute_flow($wf_curr['step_code']);
    if ($tmp_res['code']!=1) {
      $new_state_id = 3;
    }

    // free workflow
    $sql['sql'] = 'update workflow set handled_dt = null, handler_pid = null, state_id=:state_id, last_action_dt=sysdate() 
where td is null and handler_pid = :pid and workflow_id = :wf_id';
    $sql['var']['pid'] = $pid;
    $sql['var']['state_id'] = $new_state_id;
    $sql['var']['wf_id'] = $wf_curr['workflow_id'];

    $tmp_res = $this->db->exec($sql);
    if ($tmp_res['code']!=1) {
      throw new Exception("Error update workflow");
    }
    
  }

}

public function wf_add($type_id,$data) {
    $res = new \cresdata();
      $sql['sql'] = 'insert into workflow (type_id, step_code, state_id,json_data) values (:type_id, :step_code, :state_id, :json_data)';
      $sql['var']['type_id'] = $type_id;
      $sql['var']['step_code'] = 'start';
      $sql['var']['state_id'] = 1;
      $sql['var']['json_data'] = json_encode($data);

      $tmp_res = $this->db->sql_insert($sql);
print_r($tmp_res);
      if ($tmp_res['code']!=1) {
        $res->set('sql',$sql);
        throw new Exception("Error save data 1");
      }

      $this->workflow_id = $tmp_res['insert_id'];
}


public function data_get() {
  return $this->wf_data;
}

public function callback_get() {
  return $this->callback;
}

public function user_call_flag_get() {
  return $this->user_call_flag;
}

public function session_id_get() {
  return $this->fc_session_id;
}

public function node_cnt_get() {
  return $this->node_cnt;
}

public function msg_get() {
  return $this->msg;
}

public function source_id_get() {
  return $this->source_id;
}

public function user_id_get() {
  return $this->user_id;
}

  /**
   * Context allow to change object of state in runtime.
   *
   * @param State $state
   */
/*public function transitionTo(states\State $state) {
  $this->state = $state;
  $this->state->setContext($this);
} */

public function transitionToByNodeCode($node_code) {
  echo "\r\n ================================= transitionToByNodeCode :'".$node_code."'=============================\r\n";
  echo "\r\n current node : ".$this->node_code."\r\n";
  $this->node_code = $node_code;
  $this->node_cnt +=1;
  if (strlen($node_code)==0) {
    $this->is_end=true;
    return;
  }

  if ($node_code=='finish') {
    // update at db
    $sql['sql'] = 'update workflow set step_code = :step_code, td = sysdate(), last_action_dt = sysdate() where workflow_id = :wf_id and td is null';
    $sql['var']['step_code'] = $node_code;
    $sql['var']['wf_id'] = $this->workflow_id;
    print_r($sql);

    $tmp_res = $this->db->exec($sql);
    if ($tmp_res['code']!=1) {
      throw new Exception("Error update fc_session");
    }

    $this->is_end=true;
    return;
  }


  if ($node_code=='start') {
    // get first step
    $node_code = (string)$this->xml->automat->node[0][@code];
    $this->node_code = $node_code;
  }

  $node = $this->xml->xpath('//automat/node[@code=\''.$node_code.'\']');
  $step_type = (string)$node[0][@type];

  $state_class = 'workflow\\states\\'.$step_type;

  $this->state = new $state_class($node);
  $this->state->setContext($this);

  // update at db
  $sql['sql'] = 'update workflow set step_code = :step_code, last_action_dt = sysdate() where workflow_id = :wf_id and td is null';
  $sql['var']['step_code'] = $node_code;
  $sql['var']['wf_id'] = $this->workflow_id;
  print_r($sql);

  $tmp_res = $this->db->exec($sql);
  if ($tmp_res['code']!=1) {
    throw new Exception("Error update workflow");
  }
  print_r($tmp_res);
}


public function manual_steps_check() {
  try {
    $res = new \cresdata();

    $sql['sql'] = 'select * from fc_session fcs where td is null and step_code in (\'wait_operator_answer\')';
    $tmp_res = $this->db->exec($sql);

    $manual_steps = $tmp_res['values'];
    $this->user_call_flag=0;

    if (count($manual_steps)<1) {
      $res->getok();
      throw new Exception("no steps to process.");
    }

    foreach($manual_steps as $step) {
      $this->source_id = $step['src_id'];
      $this->user_id = $step['user_id'];
      $this->fc_session_id = $step['fc_session_id'];
      $node_code = $step['step_code'];

      $tmp_res = $this->execute_flow($node_code);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error execute flow");
      }

    }

    $res->getok();
  } catch (Exception $e) {
    $res->set('error_data',$tmp_res);
    $res->set('error_text',$e->getMessage());
  }
  return $res->get();
}


/*
1) проверяем наличие открытой сессии.
2) Если есть открытая сессия, то вызываем функцию шага
3) Если открытой сессии нет, то проверяем текст на соотвествие какой-либо начальной команде или тексту
*/
public function user_action($msg) {
  try {
    $res = new \cresdata();

    $this->msg = $msg;
    $sql['sql'] = 'select * from fc_session fcs where td is null and user_id = :user_id and src_id = :src_id';
    $sql['var']['src_id'] = $this->source_id;
    $sql['var']['user_id'] = $this->user_id;
    error_log(print_r($sql,1));    

    $tmp_res = $this->db->exec($sql);
    error_log(print_r($tmp_res,1));
    if ($tmp_res['code']!=1) {
      throw new Exception("Error get data 1");
    }

    $node_next = '';
    foreach($this->xml->start_commands->command as $cmd) {
      print_r($cmd);
      echo (string)$cmd->text;
      echo "\r\n";
      if ((string)$cmd->text==$msg) {
        echo (string)$cmd->to_node;
        $node_next = (string)$cmd->to_node;
        break;
      }
    }

/*    $start_phrase = (string)$this->xml->start_phrase;
    if (($start_phrase==$msg)&&(sizeof($tmp_res['values'])>0)) {
      $this->session_close();
      unset($tmp_res);
    }
*/

    if (sizeof($tmp_res['values']) > 1) {
      throw new Exception("Error session count");
    }

    if (sizeof($tmp_res['values']) == 1) {
      $this->fc_session_id = $tmp_res['values'][0]['fc_session_id'];
      $node_code = $tmp_res['values'][0]['step_code'];
    } else {
      $tmp_res = $this->flow_inititalize($msg);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error initialize flow");
      }
      $node_code = $tmp_res['node_code'];
    }

    if (strlen($node_next)==0) {
      $node_next = $node_code;
    }

    $tmp_res = $this->execute_flow($node_next);
    if ($tmp_res['code']!=1) {
      throw new Exception("Error execute flow");
    }


    $res->getok();
  } catch (Exception $e) {
    $res->set('error_data',$tmp_res);
    $res->set('error_text',$e->getMessage());
  }
  return $res->get();
}


private function execute_flow($node_code) {
  try {
    $res = new \cresdata();
    echo "\r\n============== execute_flow : ".$node_code." ==========================\r\n";
    $this->transitionToByNodeCode($node_code);

    while (!$this->is_end) {
      $node = $this->xml->xpath('//automat/node[@code=\''.$this->node_code.'\']');
      $tmp_res = $this->state->run($node[0]);
      if ($tmp_res['code']!==1) {
        print_r($tmp_res);
        throw new Exception("Error execute state");
      }
    }

    $res->getok();
  } catch (Exception $e) {
    $res->set('error_text',$e->getMessage());
  }
  return $res->get();
}


public function get_flow_list() {
    try {
      $res = new cresdata();

      $sql['sql'] = 'select * from fc_list where td is null';
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error save data 1");
      }

      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
}


private function session_close() {
    try {
      $res = new \cresdata();

      $sql['sql'] = 'update fc_session set td = sysdate() where td is null and src_id = :src_id and user_id = :user_id';
      $sql['var']['src_id'] = $this->source_id;
      $sql['var']['user_id'] = $this->user_id;

      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error at close session");
      }

      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
}

private function initialize_by_code($code) {
  try {
    $res = new \cresdata();
    // check code exists

    // load xml of workflow
    $this->xml = simplexml_load_file(site_path.'/storage/workflow/'.$code.'.xml');
 
   $res->getok();
  } catch (Exception $e) {

  }
  return $res->get();
}


private function initialize_by_type_id($type_id) {
  $res = new \cresdata();

  $sql['sql'] = 'select * from workflow_types wt where wt.wf_type_id = :type_id';
  $sql['var']['type_id'] = $type_id;
  $tmp_res = $this->db->exec($sql);
  if ($tmp_res['code']!=1) {
    throw new Exception("Error");
  }
 
  $code = $tmp_res['values'][0]['workflow_code'];
     
  $this->xml = simplexml_load_file(site_path.'/storage/workflow/'.$code.'.xml');
  $res->getok();
}

private function  info_by_type_get($type_id) {
  $sql['sql'] = "select * from workflow_types where wf_type_id = :type_id";
  $sql['var']['type_id']  = $type_id;
  
}

}