<?php
namespace bot_platform;

error_reporting(E_ALL & ~E_NOTICE);
//header('Content-type: text/html; charset=utf-8');


function autoload($class) {
  $parts = explode('\\', $class);

//  $filename = end($parts).'.php';

  $dirs = '';
  foreach ($parts as $part) {
    $dirs = $dirs.'/'.$part;
  }
  $file = ui_path.'classes'.$dirs.'.php';

//  error_log('include file:'.$file);
  if (file_exists($file) == false) {
//    error_log('ERROR');
    return false;
  }
//  error_log('OK');
  include $file;
}

spl_autoload_register('bot_platform\autoload');

//use \Exception;
class Exception extends \Exception {}

// Константы:
define('index_constant','true');

// Узнаём путь до файлов сайта

$ui_path = realpath(dirname(__FILE__) ) .'/core/';

define ('ui_path', $ui_path);

// Подключаем настройки
include $_SERVER["DOCUMENT_ROOT"]."/settings/settings.php";

define ('site_path',$_SERVER["DOCUMENT_ROOT"]);

// Создаем реестр
$registry = new \cregistry();

// Load template object
$template = new \ctemplate($registry);
$registry->set ('template', $template);

$in_arr = json_decode(file_get_contents('php://input'),true);

$srcAPI = \bot_platform\cSourceAPIFactory::createclass($in_arr['data']['source_id']);
$answer = $srcAPI->onwebhook_event($in_arr['data']['data']);

echo $answer['answer'];

?>