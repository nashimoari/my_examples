<?php
if (!defined('index_constant')) die ('Not a valid entry point');
class cquestion {
  private $db;

  function __construct() {
    $this->db = new cdb();
  }


public function mark_answer_set($qi_id,$data) {
  try {
    $res = new cresdata();

    $mark_arr = array('mark'=>$data['mark'], 'mark_text'=>$data['mark_text']);

    $utils = new cutils();
    $mark_json = $utils->json_array_convert($mark_arr);
    $mark_json = json_encode($mark_json);


    $sql['sql'] = 'update question_interface set mark_json = :mark_json where td is not null and qi_id = :qi_id';
    $sql['var'][':mark_json'] = $mark_json;
    $sql['var'][':qi_id'] = $qi_id;
    $tmp_res = $this->db->exec($sql);
    if ($tmp_res['code']!=1) {
      throw new Exception("Error save mark json");
    }

    $res->getok();
  } catch (Exception $e) {
    $res->set('error_text',$e->getMessage());
  }
  return $res->get();
}



public function mark_answer_get($qi_id) {
  try {
    $res = new cresdata();

      $sql['sql'] = "select mark_json from question_interface i where td is not null and mark_json is not null and qi_id = :qi_id";
      $sql['var'][':qi_id'] =  $qi_id;
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error");
      }

      $res->set('marked',0);
      if (count($tmp_res['values'])>0) {
        $res->set('marked',1);
        $res->set('data',(array)json_decode($tmp_res['values'][0]['mark_json']));
      }

    $res->getok();
  } catch (Exception $e) {
    $res->set('error_text',$e->getMessage());
  }
  return $res->get();
}

public function answer_json_set($fc_session_id,$arr) {
  try {
    $res = new cresdata();

    $answer_arr = array();

    foreach($arr as $answer) {
//      error_log(print_r($answer,1));
      $curr_answer = array();
      $curr_answer['answer_id'] = $answer['answer_id'];
      $curr_answer['answer_text'] = $answer['answer_text'];
      $curr_answer['question_id'] = $answer['q_id'];
      $curr_answer['question_text'] = $answer['q_text'];
      $curr_answer['koeff'] = $answer['koeff'];
      $answer_arr[] = $curr_answer;
    }

    $utils = new cutils();
    $answer_json = $utils->json_array_convert($answer_arr);
    $answer_json = json_encode($answer_json);


    $sql['sql'] = 'update question_interface set answer_json = :answer_json where td is null and fc_session_id = :fc_session_id';
    $sql['var'][':answer_json'] = $answer_json;
    $sql['var'][':fc_session_id'] = $fc_session_id;

    $tmp_res = $this->db->exec($sql);
    if ($tmp_res['code']!=1) {
      error_log(print_r($sql,1));
      error_log(print_r($tmp_res,1));
      throw new Exception("Error save answer json");
    }

    $res->getok();
  } catch (Exception $e) {
    $res->set('error_text',$e->getMessage());
  }
  return $res->get();
}


public function answer_id_set($fc_session_id,$answer_id) {
  try {
    $res = new cresdata();

    if ($answer_id <0) {
      $res->getok();
      throw new Exception("No data found");
    }

    $sql['sql'] = 'update question_interface set answer_id = :answer_id where td is null and fc_session_id = :fc_session_id';
    $sql['var'][':answer_id'] = $answer_id;
    $sql['var'][':fc_session_id'] = $fc_session_id;

    $tmp_res = $this->db->exec($sql);
    if ($tmp_res['code']!=1) {
      throw new Exception("Error save data 2");
    }

    $res->getok();
  } catch (Exception $e) {
    $res->set('error_text',$e->getMessage());
  }
  return $res->get();
}

  public function txt_from_answers_by_ngrams_get($ngram_arr) {
    try {
      $res = new cresdata();
      $check = '';
      $ngram_cnt = 0;
      foreach($ngram_arr as $n) {
        if ($ngram_cnt==0) {
          $check .=' a.answer_text like :ngram'.$ngram_cnt;
        } else {
          $check .=' or a.answer_text like :ngram'.$ngram_cnt;
        }
        $sql['var']['ngram'.$ngram_cnt] = '% '.$n.' %';
        $ngram_cnt +=1;
      }

      $sql['sql'] = 'select qb.*, a.answer_text from answer_base a,question_base qb where ('.$check.') and qb.answer_id = a.answer_id and a.td is null';
      $tmp_res = $this->db->exec($sql);

      if ($tmp_res['code']!=1) {
        $res->set('text','error to get question list');
        throw new Exception('error to get question list');
      }

      $res->set('data',$tmp_res['values']);
      $res->getok();
    } catch (Exception $e) {
      $res->set('data', $tmp_res);
    }
    return $res->get();
  }


  public function txt_from_questions_by_ngrams_get($ngram_arr) {
    try {
      $res = new cresdata();
      $check = '';
      $ngram_cnt = 0;
      foreach($ngram_arr as $n) {
        if ($ngram_cnt==0) {
          $check .=' qb.q_text like :ngram'.$ngram_cnt;
        } else {
          $check .=' or qb.q_text like :ngram'.$ngram_cnt;
        }
        $sql['var']['ngram'.$ngram_cnt] = '% '.$n.' %';
        $ngram_cnt +=1;
      }

      $sql['sql'] = 'select qb.*, a.answer_text from question_base qb, answer_base a where ('.$check.') and a.answer_id = qb.answer_id and a.td is null';
      $tmp_res = $this->db->exec($sql);

      if ($tmp_res['code']!=1) {
        $res->set('text','error to get question list');
        throw new Exception('error to get question list');
      }

      $res->set('data',$tmp_res['values']);
      $res->getok();
    } catch (Exception $e) {
      $res->set('data', $tmp_res);
    }
    return $res->get();
  }


  public function questions_count() {
    try {
      $res = new cresdata();
      $sql['sql'] = 'select count(1) cnt from question_base';
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        $res->set('text','error to get question list');
        throw new Exception('error to get question list');
      }

      $res->set('count',$tmp_res['values'][0]['cnt']);
      $res->getok();
    } catch (Exception $e) {
      $res->set('data', $tmp_res);
    }
    return $res->get();
  }

  public function get_questions_for_add_to_dictionary() {
    try {
      $res = new cresdata();
      $sql['sql'] = 'select * from question_base q where dictionary_add = 0';
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        $res->set('text','error to get question list');
        throw new Exception('error to get question list');
      }

      $res->set('data',$tmp_res['values']);
      $res->getok();
    } catch (Exception $e) {
      $res->set('data', $tmp_res);
    }
    return $res->get();
  }


  public function wait_moderation_get() {
    try {
      $res = new cresdata();
      $sql['sql'] = 'select 
(select user_id from fc_session fs where fs.fc_session_id = q.fc_session_id) user_id,
q.*,timediff (sysdate(),q.fd) duration
,(select a.answer_text from answer_base a where a.answer_id = q.answer_id and a.td is null) answer_text
from question_interface q where td is null limit 0,5';
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        $res->set('text','error to get question list');
        throw new Exception('error to get question list');
      }

      $res->set('data',$tmp_res['values']);
      $res->getok();
    } catch (Exception $e) {
      $res->set('data', $tmp_res);
    }
    return $res->get();
  }

  public function answer_set($data) {
    try {
      $res = new cresdata();
      $this->db->connect_nocommit();

      $usm_client = cusmFactory::createclass();

      $sql['sql'] = "select count(1) cnt from question_interface i where i.qi_id = :id and td is null";
      $sql['var'][':id'] =  $data['q_id'];
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error");
      }
      unset($sql);

      if ($tmp_res['values'][0]['cnt'] !=1) {
        throw new Exception("Already answered");      
      }

      // get question text
      $sql['sql'] = "select q_text from question_interface i where i.qi_id = :id and td is null";
      $sql['var'][':id'] =  $data['q_id'];
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error");
      }
      unset($sql);
      $q_text = $tmp_res['values'][0]['q_text'];

      $tmp_res = $usm_client->current_userid_get();
      if ($tmp_res['code']!=1) {
        throw new Exception("Error");
      }

      $user_id = $tmp_res['user_id'];
      if ($user_id <0) {
        throw new Exception("User not authenticated");
      }

      //answer add
      if ($data['answer_id']>0) {
        $answer_id = $data['answer_id'];
      } else {
        $tmp_res = $this->answer_add($data);

        if ($tmp_res['code']!=1) {
          throw new Exception("Error save data 2");
        }
        $answer_id = $tmp_res['answer_id'];
      }

      // question add to base
      if ($data['qa_add_flag']) {
        $arg['q_text'] = $q_text;
        $arg['answer_id'] = $answer_id;
        $tmp_res = $this->base_question_add($arg);
        if ($tmp_res['code']!=1) {
          throw new Exception("Error save question to base");
        }
      }

      $sql['sql'] = 'update question_interface set answer_id = :answer_id, td = sysdate(), wait_moderation = 0 where td is null and qi_id = :id';
      $sql['var'][':answer_id'] = $answer_id;
      $sql['var'][':id'] = $data['q_id'];

      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error save data 2");
      }

      $this->db->commit();
      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
  }


  public function question_add($data) {
    try {
      $res = new cresdata();

      $sql['sql'] = 'insert into question_interface (q_text,fc_session_id,wait_moderation) values (:q_text,:fc_session_id, 1)';
      $sql['var'][':q_text'] = iconv("UTF-8","cp1251", $data['text']);
      $sql['var'][':fc_session_id'] = $data['fc_session_id'];
      $tmp_res = $this->db->sql_insert($sql);

      if ($tmp_res['code']!=1) {
        throw new Exception("Error save data 2");
      }

      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
  }

public function answer_to_send_check($fc_sess_id) {
      $sql['sql'] = 'select count(1) cnt from question_interface qi 
where dt_send_handled is null and dt_sended is null and wait_moderation = 0 
and fc_session_id = :fc_sess_id and td is not null';
      $sql['var'][':fc_sess_id'] = $fc_sess_id;
      $tmp_res = $this->db->exec($sql);

      if ($tmp_res['code']!=1) {
        throw new Exception("Error 1");
      }

  return $tmp_res['values'][0]['cnt'];
}

  public function answer_to_send_by_fc_sess_get($fc_sess_id) {
    try {
      $res = new cresdata();

      $sql['sql'] = 'select q.qi_id, q.q_text, a.answer_text
from question_interface q
,answer_base a
where 
fc_session_id = :fc_sess_id
and a.answer_id = q.answer_id';

      $sql['var'][':fc_sess_id'] = $fc_sess_id;
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error 2");
      }

      $res->set('data',$tmp_res['values']);
      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
  }

  public function question_and_answer_add($data) {
    try {
      $res = new \cresdata();
      $usm = cusmFactory::createclass();

      $tmp_res = $usm->current_userid_get();
      if ($tmp_res['code']!=1) {
        throw new Exception("Error");
      }

      $user_id = $tmp_res['user_id'];
      if ($user_id <0) {
        throw new Exception("User not authenticated");
      }

      $is_final = 0;
      if ($data['is_final']==1) { $is_final=1; }

      $answer_detail = '';
      if (isset($data['answer_detail'])) {
        $answer_detail = iconv("UTF-8","cp1251", $data['answer_detail']);
      }

      $sql['sql'] = "insert into answer_base (answer_text,answer_detail,user_id, is_final) values (:answer_text, :answer_detail, :user_id, :is_final)";
      $sql['var'][':answer_text'] = iconv("UTF-8","cp1251", $data['answer_text']);
      $sql['var'][':user_id'] = $user_id;
      $sql['var'][':answer_detail'] = $answer_detail;
      $sql['var'][':is_final'] = $is_final;
      $tmp_res = $this->db->sql_insert($sql);

     if ($tmp_res['code']!=1) {
        throw new Exception("Error save data 1");
      }
      unset($sql);

      $answer_id = $tmp_res['insert_id'];

      $sql['sql'] = 'insert into question_base (q_text,answer_id) values (:q_text, :answer_id)';
      $sql['var'][':q_text'] = iconv("UTF-8","cp1251", $data['question_text']);
      $sql['var'][':answer_id'] = $answer_id;
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        throw new Exception("Error save data 2");
      }

      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
  }


  private function answer_add($data) {
    try {
      $res = new \cresdata();

      $usm_client = cusmFactory::createclass();

      $tmp_res = $usm_client->current_userid_get();
      if ($tmp_res['code']!=1) {
        throw new Exception("Error");
      }

      $user_id = $tmp_res['user_id'];
      if ($user_id <0) {
        throw new Exception("User not authenticated");
      }

      $is_final = 0;
      if ($data['is_final']==1) { $is_final=1; }
      $sql['sql'] = "insert into answer_base (answer_text,fd,user_id, is_final) values (:answer_text,sysdate(), :user_id, :is_final)";
      $sql['var'][':answer_text'] = iconv("UTF-8","cp1251", $data['answer_text']);
      $sql['var'][':user_id'] = $user_id;
      $sql['var'][':is_final'] = $is_final;
      $tmp_res = $this->db->sql_insert($sql);


     if ($tmp_res['code']!=1) {
         throw new Exception("Error save data 1");
      }

      $res->set('answer_id',$tmp_res['insert_id']);

      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
  }

  private function base_question_add($data) {
    try {
      $res = new \cresdata();
      $q_text = $data['q_text'];

      $sql['sql'] = 'insert into question_base (q_text,answer_id) values (:q_text, :answer_id)';
      $sql['var'][':q_text'] = $q_text;
      $sql['var'][':answer_id'] = $data['answer_id'];
      $tmp_res = $this->db->exec($sql);
      if ($tmp_res['code']!=1) {
        error_log(print_r($sql,1));
        error_log(print_r($tmp_res,1));
        throw new Exception("Error save data 2");
      }
      $question_id = $tmp_res['insert_id'];

      $res->set('question_id',$question_id);

      $cngram = new \ngram_dictionary($q_text);
      $cngram->db_set($this->db);
      $tmp_res = $cngram->text_add();

      $res->getok();
    } catch (Exception $e) {
      $res->set('error_text',$e->getMessage());
    }
    return $res->get();
  }


}